﻿using Aspose.Words;
using BitMiracle.Docotic.Pdf;
using Microsoft.Office.Interop.Word;
using Newtonsoft.Json;
using NopHoSoMotCuaZaLo.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using static NopHoSoMotCuaZaLo.Models.ZaloInfomation;
using System.Xml;
using Run = DocumentFormat.OpenXml.Wordprocessing.Run;
using BookmarkStart = DocumentFormat.OpenXml.Wordprocessing.BookmarkStart;
using WordToPDF;
using Format = NopHoSoMotCuaZaLo.Models.Format;
using System.Diagnostics;

namespace NopHoSoMotCuaZaLo.Controllers
{
    public class NopHoSoZaLoController : Controller
    {
        String pathUploadFTP = ConfigurationManager.AppSettings["pathUploadFTP"];
        String ftpServerIP = ConfigurationManager.AppSettings["ftpServerIP"];
        String ftpUserID = ConfigurationManager.AppSettings["ftpUserID"];
        String ftpPassword = ConfigurationManager.AppSettings["ftpPassword"];
        String messktDidong = ConfigurationManager.AppSettings["messktDidong"];
        String messktFiletailen = ConfigurationManager.AppSettings["messktFiletailen"];
        String nameHinhThuc1 = ConfigurationManager.AppSettings["nameHinhThuc1"];
        String nameHinhThuc2 = ConfigurationManager.AppSettings["nameHinhThuc2"];
        String tokenZalo = ConfigurationManager.AppSettings["ToKenkeyZalo"];
        String messZalo = ConfigurationManager.AppSettings["messZaloTC"];
        String messBC = ConfigurationManager.AppSettings["messBCCI"];
        String CustomerCode = ConfigurationManager.AppSettings["CustomerCode"];
        String TokenkeyVNPost = ConfigurationManager.AppSettings["TokenkeyVNPost"];
        String APILink = ConfigurationManager.AppSettings["APILink"];


        ConnectDataDichVuCongDataContext dataDichvucong = new ConnectDataDichVuCongDataContext();
        ConnectDataFpt1gatetayninhDataContext dataFpt1gatetayninh = new ConnectDataFpt1gatetayninhDataContext();
        ConnectDataFptGateMasterDataContext dataFptGatemaster = new ConnectDataFptGateMasterDataContext();


        //Lưu thông tin ảnh upload 
        public List<ArrayFile> GetArrayImg()
        {
            List<ArrayFile> file_Array = Session["File_Array"] as List<ArrayFile>;
            if (file_Array == null)
            {
                ///neu chưa tồn tại session thì tạo
                file_Array = new List<ArrayFile>();
                Session["File_Array"] = file_Array;
            }
            return file_Array;
        }
        //Lưu thông tin pdf convert
        public List<ArrayFile> GetArrayPdf()
        {
            List<ArrayFile> file_Array = Session["File_Array_Pdf"] as List<ArrayFile>;
            if (file_Array == null)
            {
                ///neu chưa tồn tại session thì tạo
                file_Array = new List<ArrayFile>();
                Session["File_Array_Pdf"] = file_Array;
            }
            return file_Array;
        }
        public List<ArrayFile> GetArrayTam()
        {
            List<ArrayFile> file_Array = Session["File_Array_Tam"] as List<ArrayFile>;
            if (file_Array == null)
            {
                ///neu chưa tồn tại session thì tạo
                file_Array = new List<ArrayFile>();
                Session["File_Array_Tam"] = file_Array;
            }
            return file_Array;
        }
        //Thêm ảnh upload vào session
        public void AddImg(string name, int idhoso)
        {
            List<ArrayFile> file_Array = GetArrayImg();
            ArrayFile name1 = file_Array.Find(n => n.utenanh == name);
            if (name1 == null)
            {
                name1 = new ArrayFile(name, idhoso);
                file_Array.Add(name1);
            }
        }
        //Thêm pdf convert vào session
        public void AddPdf(string name, int idhoso)
        {
            List<ArrayFile> file_Array = GetArrayPdf();
            ArrayFile name1 = file_Array.Find(n => n.utenanh == name);
            if (name1 == null)
            {
                name1 = new ArrayFile(name, idhoso);
                file_Array.Add(name1);
            }
        }
        public void AddTam(string name, int idhoso)
        {
            List<ArrayFile> file_Array = GetArrayTam();
            ArrayFile name1 = file_Array.Find(n => n.utenanh == name);
            if (name1 == null)
            {
                name1 = new ArrayFile(name, idhoso);
                file_Array.Add(name1);
            }
        }
        public void RemoveImg(string name)
        {
            List<ArrayFile> file_Array = GetArrayImg();
            ArrayFile name1 = file_Array.SingleOrDefault(n => n.utenanh == name);
            if (name1 != null)
            {
                file_Array.RemoveAll(n => n.utenanh == name);
            }
        }
        public void RemoveSession()
        {
            //Lay tu Session
            List<ArrayFile> file_Array = GetArrayImg();
            List<ArrayFile> file_Array_pdf = GetArrayPdf();
            file_Array.Clear();
            file_Array_pdf.Clear();
        }
        // GET: NopHoSoZaLo
        public ActionResult Index()
        {
            return View();
        }

        // GET: NopHoSoZaLo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        public ActionResult SaveUploadedFile(int idHoSo)
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            string finalFileName = "";
            foreach (string fileName in Request.Files)
            {
                HttpPostedFileBase file = Request.Files[fileName];
                //Save file content goes here
                fName = file.FileName;
                if (file != null && file.ContentLength > 0)
                {
                    var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
                    string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "user");
                    var fileName1 = Path.GetFileName(file.FileName);
                    string ngaythang = DateTime.Now.Ticks.ToString();
                    finalFileName = ngaythang + "_" + file.FileName;
                    bool isExists = System.IO.Directory.Exists(pathString);
                    if (!isExists)
                        System.IO.Directory.CreateDirectory(pathString);

                    var path = string.Format("{0}\\{1}", pathString, file.FileName);
                    file.SaveAs(path);
                    AddImg(file.FileName, idHoSo);
                }

            }

            if (isSavedSuccessfully)
            {
                return Json(new { Message = finalFileName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }
        [HttpPost]
        public ActionResult DeleteFileUpload(string FileName)
        {
            bool isDelSuccessfully = false;

            var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
            string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "user");
            var fName = "";
            fName = FileName;
            var fullPath = string.Format("{0}\\{1}", pathString, fName);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
                isDelSuccessfully = true;
                RemoveImg(fName);
            }
            if (isDelSuccessfully)
            {
                return Json(new { Message = "succesful" });
            }
            else
            {
                return Json(new { Message = "Error in del file" });
            }
        }
        public string imagesToPdf(List<ArrayFile> images)
        {
            using (PdfDocument pdf = new PdfDocument())
            {
                foreach (var item in images)
                {
                    if (images.IndexOf(item) > 0)
                        pdf.AddPage();
                    PdfPage page = pdf.Pages[images.IndexOf(item)];
                    string imagePath = item.utenanh;

                    var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
                    string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "user");
                    var fName = "";
                    fName = imagePath;
                    var fullPath = string.Format("{0}\\{1}", pathString, fName);


                    using (Bitmap bitmap = new Bitmap(fullPath))
                    {
                        //Orientation Rotate 180
                        //bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        //Orientation	Rotate 90 CW
                        //bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                        RotateImg.ExifRotate(bitmap);
                        PdfImage pdfImage = pdf.AddImage(bitmap);
                        page.Width = pdfImage.Width;
                        page.Height = pdfImage.Height;
                        page.Canvas.DrawImage(pdfImage, 0, 0);

                    }
                }
                var originalDirectory1 = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
                string pathString1 = System.IO.Path.Combine(originalDirectory1.ToString(), "user");
                var fileName1 = Path.GetFileName("HosoKemTheo.pdf");
                string ngaythang = DateTime.Now.Ticks.ToString();
                string finalFileName = ngaythang + "_" + fileName1;
                var fullPath1 = string.Format("{0}\\{1}", pathString1, finalFileName);
                pdf.Save(fullPath1);
                return finalFileName;
            }
        }
        //public void ConverToPdf(List<ArrayFile> images)
        //{
        //    Document doc = new Document();
        //    DocumentBuilder builder = new DocumentBuilder(doc);
        //    foreach (var item in images)
        //    {
        //        string imagePath = item.utenanh;
        //        var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
        //        string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "user");
        //        var fName = "";
        //        fName = imagePath;
        //        var fullPath = string.Format("{0}\\{1}", pathString, fName);

        //        System.Drawing.Image img = System.Drawing.Image.FromFile(fullPath);
        //        //img.RotateFlip(RotateFlipType.Rotate270FlipNone);
        //        PageSetup pageSetup = builder.PageSetup;
        //        pageSetup.PageWidth = ConvertUtil.PixelToPoint(img.Width, img.HorizontalResolution);
        //        pageSetup.PageHeight = ConvertUtil.PixelToPoint(img.Height, img.VerticalResolution);
        //        builder.InsertImage(img, Aspose.Words.Drawing.RelativeHorizontalPosition.Page, 0, Aspose.Words.Drawing.RelativeVerticalPosition.Page, 0,
        //            pageSetup.PageWidth, pageSetup.PageHeight, Aspose.Words.Drawing.WrapType.None);
        //        builder.InsertBreak(BreakType.SectionBreakNewPage);
        //    }
        //    var originalDirectory1 = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
        //    string pathString1 = System.IO.Path.Combine(originalDirectory1.ToString(), "user");
        //    var fileName1 = Path.GetFileName("conver.pdf");
        //    string ngaythang = DateTime.Now.Ticks.ToString();
        //    string finalFileName = ngaythang + "_" + fileName1;
        //    var fullPath1 = string.Format("{0}\\{1}", pathString1, finalFileName);
        //    doc.Save(fullPath1);
        //    Session["LinkPDF"] = finalFileName;
        //}
        public void UploadFileToFTP(List<ArrayFile> image)
        {
            //String ftpServerIP = "ftp://192.168.20.12";
            //String ftpUserID = "admin";
            //String ftpPassword = "admin";
            //String filePath = "D://chihang.jpg";
            foreach (var item in image)
            {
                string imagePath = item.utenanh;
                var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
                string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "user");
                var fName = "";
                fName = imagePath;
                var fullPath = string.Format("{0}\\{1}", pathString, fName);

                //String ext = new FileInfo(filePath).Extension;
                // Let's create dynamically a filename from datetime
                //string dynamicFileName = String.Format("{0:yyyyMMdd_HHmmssms}{1}", DateTime.Now, ext);

                String ftpFilePath = String.Format("Alfresco/TayNinh/{0}", item.utenanh.Replace("_", "☺"));
                FtpWebRequest ftpRequest;
                ftpRequest = (FtpWebRequest)WebRequest.Create(
                  String.Format("{0}/{1}", ftpServerIP, ftpFilePath));
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

                FileStream stream = System.IO.File.OpenRead(pathUploadFTP + item.utenanh);
                byte[] buffer = new byte[stream.Length];

                stream.Read(buffer, 0, buffer.Length);
                stream.Close();

                Stream reqStream = ftpRequest.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Close();
            }

        }
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadDSDonVi(string tpid)
        {
            if (string.IsNullOrEmpty(tpid))
                return Json(HttpNotFound());
            var categoryList = GetDonViList(Convert.ToInt32(tpid));
            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.TenDonVi,
                Value = m.DonViID.ToString()
            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }
        private IList<SON_Get95XaPhuong> GetDonViList(int tpid)
        {
            return dataFptGatemaster.SON_Get95XaPhuongs.OrderBy(c => c.TenDonVi).Where(c => c.DonViCapChaID == tpid).ToList();
        }
        // GET: NopHoSoZaLo/Create
        public ActionResult Create(long donvi, long loaihs, long linhvuc)
        {
            //long donvi = long.Parse(s_donvi);
            //long loaihs = long.Parse(s_loaihs);
            ViewBag.HinhThuc1 = nameHinhThuc1;
            ViewBag.HinhThuc2 = nameHinhThuc2;
            DMLOAIHOSO dm = dataFpt1gatetayninh.DMLOAIHOSOs.SingleOrDefault(i => i.LoaiHoSoID == loaihs);
            ViewBag.TenHoSo = dm.TenLoaiHoSo;
            ViewBag.MaLoaiHoSo = loaihs;
            ViewBag.MaLoaiDonVi = donvi;
            ViewBag.LinhVuc = linhvuc;
            DMSOBANNGANH ten = dataFpt1gatetayninh.DMSOBANNGANHs.SingleOrDefault(i => i.SoBanNganhID == donvi);
            if (ten is null)
            {
                DM_DONVI ten1 = dataFptGatemaster.DM_DONVIs.SingleOrDefault(i => i.DonViID == donvi);
                ViewBag.TenDonVi = ten1.TenDonVi;
            }
            else
            {
                ViewBag.TenDonVi = ten.TenSoBanNganh;
            }

            //var danhsach = dataFpt1gatetayninh.GETDANHSACHHOSOKEMTHEO_BY_LOAIHOSO(loaihs).ToList();
            var danhsach = dataFpt1gatetayninh.SON_GETDANHSACHHOSOKEMTHEO_BY_LOAIHOSO_1(loaihs, donvi).ToList();
            //var danhsach = dataFpt1gatetayninh.SON_GETDANHSACHHOSOKEMTHEO_BY_LOAIHOSO_V2(loaihs, donvi).ToList();
            //var danhsach = dataDichvucong.sp_GetDanhSachHoSoKemTheo(donvi,loaihs).ToList();
            ViewData["LoaiGiayTo"] = new SelectList(dataFptGatemaster.DMLOAIGIAYTOs.ToList().OrderBy(n => n.LoaiGiayToID), "LoaiGiayToID", "TenLoaiGiayTo");
            ViewBag.Huyen = dataFptGatemaster.Son_GetQuanHuyens.ToList().OrderBy(n => n.TenDonVi);
            //ViewData["XaPhuong"] = new SelectList(dataFptGatemaster.SON_Get95XaPhuongs.ToList().OrderBy(n => n.TenDonVi), "DonViID", "TenDonVi");
            return View(danhsach);
        }

        // POST: NopHoSoZaLo/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection, DVC_BCCI bc)
        {
            //try
            //{
            //    // TODO: Add insert logic here

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            var iddonvi = int.Parse(collection["MaDonVi"]);
            var idhoso = int.Parse(collection["MaLoaiHoSo"]);
            var hoten = textInfo.ToTitleCase(collection["HoTen"]);
            var lv = int.Parse(collection["LinhVuc"]);
            var sogiayto = collection["SoGiayTo"];
            var loaigiayto = int.Parse(collection["LoaiGiayTo"]);
            var didong = collection["DiDong"];
            var hinhthuc = int.Parse(collection["HinhThuc"]);
            var diachi = collection["DiaChi"];
            var ghichu = collection["GhiChu"];
            if (hinhthuc == 1)
            {
                //hình thức 1: zalo
                var mahoso = dataDichvucong.DVC_HOSO_Insert_V3(0, lv, idhoso, iddonvi, "THS", hoten, 0, 2, "", "", "", loaigiayto,
                    sogiayto, DateTime.Now, "", "", didong, "", "", 703, 715, 770, "", DateTime.Now, DateTime.Now, null, 1);

                List<ArrayFile> file_Array = GetArrayImg();
                List<ArrayFile> Sapxep = file_Array.OrderByDescending(a => a.uidhoso).ToList();
                List<ArrayFile> file_Array_Tam = GetArrayTam();
                AddTam(Sapxep[0].utenanh, Sapxep[0].uidhoso);
                for (var i = 0; i < Sapxep.Count() - 1; i++)
                {
                    if (Sapxep[i].uidhoso != Sapxep[i + 1].uidhoso)
                    {
                        AddPdf(imagesToPdf(file_Array_Tam), Sapxep[i].uidhoso);
                        file_Array_Tam.Clear();
                        AddTam(Sapxep[i + 1].utenanh, Sapxep[i + 1].uidhoso);
                    }
                    else
                    {
                        AddTam(Sapxep[i + 1].utenanh, Sapxep[i + 1].uidhoso);
                    }
                }
                AddPdf(imagesToPdf(file_Array_Tam), file_Array_Tam[0].uidhoso);
                file_Array_Tam.Clear();

                List<ArrayFile> file_Array_pdf = GetArrayPdf();

                UploadFileToFTP(file_Array_pdf);
                foreach (var item in file_Array_pdf)
                {
                    var id = dataDichvucong.sp_DVC_HOSOKEMTHEO_Insert_V4(mahoso, item.uidhoso, item.utenanh, item.utenanh.Replace("_", "☺"), "1000", 0, DateTime.Now, null, null);
                    //DeleteFileUpload(item.utenanh);
                }
                //ConverToPdf(file_Array);
                send(messZalo, didong);
                RemoveSession();
                Session["ThongTinHinhThuc"] = nameHinhThuc1;
            }
            if (hinhthuc == 2)
            {
                var idxa = int.Parse(collection["IDXA"]);
                var idhuyen = int.Parse(collection["IDQUAN"]);
                bc.PhuongID = idxa;
                bc.QuanID = idhuyen;
                bc.IDDonVi = iddonvi;
                bc.LoaiHoSo = idhoso;
                bc.LinhVuc = lv;
                bc.HoTen = hoten;
                bc.SoDT = didong;
                bc.SoGT = sogiayto;
                bc.LoaiGT = loaigiayto;
                bc.NgayNop = DateTime.Now;
                bc.GhiChu = ghichu;
                bc.TrangThai = 1;
                bc.DiaChi = diachi;
                dataDichvucong.DVC_BCCIs.InsertOnSubmit(bc);
                dataDichvucong.SubmitChanges();
                Session["ThongTinHinhThuc"] = nameHinhThuc2;
                DMLOAIHOSO dm = dataFpt1gatetayninh.DMLOAIHOSOs.SingleOrDefault(i => i.LoaiHoSoID == idhoso);
                var xp = dataFptGatemaster.SON_Get95XaPhuongs.SingleOrDefault(n => n.DonViID == idxa);
                //var qh = dataFptGatemaster.Son_GetQuanHuyens.SingleOrDefault(n => n.DonViID == idhuyen);
                sentVNPost(idhoso.ToString(), idhuyen, diachi +" - "+ xp.TenDonVi, hoten, didong, idhoso + "||" + dm.TenLoaiHoSo, "Nộp qua Zalo - " + ghichu, "TTHCC", "Diachi");
                send(messBC, didong);
            }
            Session["ThongTinTen"] = hoten;
            Session["ThongTinDT"] = didong;
            return RedirectToAction("KetQua"); ;
        }
        public ActionResult Desktopdevice()
        {
            return View();
        }
        public ActionResult CreateAppTayNinh(long donvi, long loaihs, long linhvuc)
        {

            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex OS = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex device = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string device_info = string.Empty;
            if (OS.IsMatch(userAgent))
            {
                device_info = OS.Match(userAgent).Groups[0].Value;
            }
            if (device.IsMatch(userAgent.Substring(0, 4)))
            {
                device_info += device.Match(userAgent).Groups[0].Value;
            }
            if (!string.IsNullOrEmpty(device_info))
            {
                //ViewBag.DeviceInfo = "You are using a Mobile device. " + device_info;
                //long donvi = long.Parse(s_donvi);
                //long loaihs = long.Parse(s_loaihs);
                ViewBag.HinhThuc1 = nameHinhThuc1;
                ViewBag.HinhThuc2 = nameHinhThuc2;
                DMLOAIHOSO dm = dataFpt1gatetayninh.DMLOAIHOSOs.SingleOrDefault(i => i.LoaiHoSoID == loaihs);
                ViewBag.TenHoSo = dm.TenLoaiHoSo;
                ViewBag.MaLoaiHoSo = loaihs;
                ViewBag.MaLoaiDonVi = donvi;
                ViewBag.LinhVuc = linhvuc;
                DMSOBANNGANH ten = dataFpt1gatetayninh.DMSOBANNGANHs.SingleOrDefault(i => i.SoBanNganhID == donvi);
                if (ten is null)
                {
                    DM_DONVI ten1 = dataFptGatemaster.DM_DONVIs.SingleOrDefault(i => i.DonViID == donvi);
                    ViewBag.TenDonVi = ten1.TenDonVi;
                }
                else
                {
                    ViewBag.TenDonVi = ten.TenSoBanNganh;
                }

                //var danhsach = dataFpt1gatetayninh.GETDANHSACHHOSOKEMTHEO_BY_LOAIHOSO(loaihs).ToList();
                var danhsach = dataFpt1gatetayninh.SON_GETDANHSACHHOSOKEMTHEO_BY_LOAIHOSO_1(loaihs, donvi).ToList();
                //var danhsach = dataFpt1gatetayninh.SON_GETDANHSACHHOSOKEMTHEO_BY_LOAIHOSO_V2(loaihs, donvi).ToList();
                //var danhsach = dataDichvucong.sp_GetDanhSachHoSoKemTheo(donvi,loaihs).ToList();
                ViewData["LoaiGiayTo"] = new SelectList(dataFptGatemaster.DMLOAIGIAYTOs.ToList().OrderBy(n => n.LoaiGiayToID), "LoaiGiayToID", "TenLoaiGiayTo");
                ViewBag.Huyen = dataFptGatemaster.Son_GetQuanHuyens.ToList().OrderBy(n => n.TenDonVi);
                //ViewData["XaPhuong"] = new SelectList(dataFptGatemaster.SON_Get95XaPhuongs.ToList().OrderBy(n => n.TenDonVi), "DonViID", "TenDonVi");
                return View(danhsach);
            }
            else
            {
                return RedirectToAction("Desktopdevice");
            }
            
        }

        // POST: NopHoSoZaLo/Create
        [HttpPost]
        public ActionResult CreateAppTayNinh(FormCollection collection, DVC_BCCI bc)
        {
            //try
            //{
            //    // TODO: Add insert logic here

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            var iddonvi = int.Parse(collection["MaDonVi"]);
            var idhoso = int.Parse(collection["MaLoaiHoSo"]);
            var hoten = textInfo.ToTitleCase(collection["HoTen"]);
            var lv = int.Parse(collection["LinhVuc"]);
            var sogiayto = collection["SoGiayTo"];
            var loaigiayto = int.Parse(collection["LoaiGiayTo"]);
            var didong = collection["DiDong"];
            var hinhthuc = int.Parse(collection["HinhThuc"]);
            var diachi = collection["DiaChi"];
            var ghichu = collection["GhiChu"];
            if (hinhthuc == 1)
            {
                //hình thức 12: SieuApp
                var mahoso = dataDichvucong.DVC_HOSO_Insert_V3(0, lv, idhoso, iddonvi, "THS", hoten, 0, 2, "", "", "", loaigiayto,
                    sogiayto, DateTime.Now, "", "", didong, "", "", 703, 715, 770, "", DateTime.Now, DateTime.Now, null, 3);

                List<ArrayFile> file_Array = GetArrayImg();
                List<ArrayFile> Sapxep = file_Array.OrderByDescending(a => a.uidhoso).ToList();
                List<ArrayFile> file_Array_Tam = GetArrayTam();
                AddTam(Sapxep[0].utenanh, Sapxep[0].uidhoso);
                for (var i = 0; i < Sapxep.Count() - 1; i++)
                {
                    if (Sapxep[i].uidhoso != Sapxep[i + 1].uidhoso)
                    {
                        AddPdf(imagesToPdf(file_Array_Tam), Sapxep[i].uidhoso);
                        file_Array_Tam.Clear();
                        AddTam(Sapxep[i + 1].utenanh, Sapxep[i + 1].uidhoso);
                    }
                    else
                    {
                        AddTam(Sapxep[i + 1].utenanh, Sapxep[i + 1].uidhoso);
                    }
                }
                AddPdf(imagesToPdf(file_Array_Tam), file_Array_Tam[0].uidhoso);
                file_Array_Tam.Clear();

                List<ArrayFile> file_Array_pdf = GetArrayPdf();

                UploadFileToFTP(file_Array_pdf);
                foreach (var item in file_Array_pdf)
                {
                    var id = dataDichvucong.sp_DVC_HOSOKEMTHEO_Insert_V4(mahoso, item.uidhoso, item.utenanh, item.utenanh.Replace("_", "☺"), "1000", 0, DateTime.Now, null, null);
                    //DeleteFileUpload(item.utenanh);
                }
                //ConverToPdf(file_Array);
                send(messZalo, didong);
                RemoveSession();
                Session["ThongTinHinhThuc"] = nameHinhThuc1;
            }
            if (hinhthuc == 2)
            {
                var idxa = int.Parse(collection["IDXA"]);
                var idhuyen = int.Parse(collection["IDQUAN"]);
                bc.PhuongID = idxa;
                bc.QuanID = idhuyen;
                bc.IDDonVi = iddonvi;
                bc.LoaiHoSo = idhoso;
                bc.LinhVuc = lv;
                bc.HoTen = hoten;
                bc.SoDT = didong;
                bc.SoGT = sogiayto;
                bc.LoaiGT = loaigiayto;
                bc.NgayNop = DateTime.Now;
                bc.GhiChu = ghichu;
                bc.TrangThai = 1;
                bc.DiaChi = diachi;
                dataDichvucong.DVC_BCCIs.InsertOnSubmit(bc);
                dataDichvucong.SubmitChanges();
                Session["ThongTinHinhThuc"] = nameHinhThuc2;
                DMLOAIHOSO dm = dataFpt1gatetayninh.DMLOAIHOSOs.SingleOrDefault(i => i.LoaiHoSoID == idhoso);
                var xp = dataFptGatemaster.SON_Get95XaPhuongs.SingleOrDefault(n => n.DonViID == idxa);
                //var qh = dataFptGatemaster.Son_GetQuanHuyens.SingleOrDefault(n => n.DonViID == idhuyen);
                sentVNPost(idhoso.ToString(), idhuyen, diachi + " - " + xp.TenDonVi, hoten, didong, idhoso + "||" + dm.TenLoaiHoSo, "Nộp qua Zalo - " + ghichu, "TTHCC", "Diachi");
                send(messBC, didong);
            }
            Session["ThongTinTen"] = hoten;
            Session["ThongTinDT"] = didong;
            return RedirectToAction("KetQua"); 
        }
        public void DownloadFtp(string filename)
        {
            // Get the object used to communicate with the server.
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpServerIP + "/Alfresco/TayNinh/" + filename);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            //your fav write method:
            var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
            string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "BieuMau");
            var fullPath = string.Format("{0}\\{1}", pathString, filename);

            using (var stream = System.IO.File.Create(fullPath))
            {
                responseStream.CopyTo(stream);
            }
        }
        
        public ActionResult TaiFile(String namefile)
        {
            DownloadFtp(namefile);

            var originalDirectory = new DirectoryInfo(string.Format("{0}FileTaiLen", Server.MapPath(@"\")));
            string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "BieuMau");
            var fullPath = string.Format("{0}\\{1}", pathString, namefile);

            System.IO.FileInfo file = new System.IO.FileInfo(fullPath);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(file.FullName);
            Response.End();

            var data = new { Status = "Success" };
            return Json(data);
        }
        public JsonResult KTFileTaiLen()
        {
            List<ArrayFile> file_Array = GetArrayImg();
            if (file_Array.Count() > 0)
            {
                //foreach(var item in file_Array)
                //{
                //    //.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF
                //    string[] mangChuoi = new string[] { "jpeg", "jpg", "png", "gif" , "JPEG", "JPG", "PNG", "GIF" };
                //    string[] arrListStr = item.utenanh.Split('.');
                //    foreach (string chuoi in mangChuoi)
                //    {
                //        if(String.Compare(chuoi, arrListStr[1], true) != 0)
                //        {
                //            return Json(new { Message = "Chỉ chấp nhận file ảnh! vui lòng kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                //        }
                //    }
                //}
                return Json(new { Message = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Message = messktFiletailen }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult KTSDT(string sdt)
        {
            if (sdt.Length == 10)
            {
                return Json(new { Message = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Message = messktDidong }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult KetQua()
        {
            ViewBag.ThongTinTen = Session["ThongTinTen"].ToString();
            ViewBag.ThongTinDT = Session["ThongTinDT"].ToString();
            ViewBag.ThongTinHinhThuc = Session["ThongTinHinhThuc"].ToString();
            return View();
        }
        public ActionResult DanhSachHuyen(long loaihs, long linhvuc)
        {
            ViewBag.LLHS = loaihs;
            ViewBag.LLV = linhvuc;
            //String idLoaiHuyen = ConfigurationManager.AppSettings["idLoaiHuyen"];
            //String idLinhVucHuyen = ConfigurationManager.AppSettings["idLinhVucHuyen"];
            //ViewBag.idLinhVucHuyen = idLinhVucHuyen;
            //ViewBag.idLoaiHuyen = idLoaiHuyen;
            //var ds = dataFpt1gatetayninh.DMSOBANNGANHs.ToList().Where(n=>n.CapDonViID==2).OrderBy(n => n.TenSoBanNganh);
            var ds = dataFptGatemaster.Son_GetQuanHuyens.ToList().OrderBy(n => n.TenDonVi);
            return View(ds);
        }
        public ActionResult XaPhuongCap1(long loaihs, long linhvuc)
        {
            ViewBag.LLHS = loaihs;
            ViewBag.LLV = linhvuc;
            var ds = dataFptGatemaster.DMQUANs.ToList().OrderBy(n => n.TenQuan);
            return View(ds);
        }
        public ActionResult XaPhuong(int idquan, long loaihs, long linhvuc)
        {
            ViewBag.LLHS = loaihs;
            ViewBag.LLV = linhvuc;
            var ds = dataFptGatemaster.DM_DONVIs.ToList().Where(n => n.QuanHuyenID == idquan & n.Cap == 3);
            //var ds = dataFptGatemaster.DMPHUONGs.ToList().Where(n => n.QuanID == idquan).OrderByDescending(n => n.TenPhuong);
            return View(ds);
        }
        [HttpGet]
        public ActionResult LinkXaPhuong(string tenxaphuong, int idquan, long loaihs, long linhvuc)
        {
            ViewBag.LLHS = loaihs;
            ViewBag.LLV = linhvuc;
            var link = dataFptGatemaster.DM_DONVIs.SingleOrDefault(n => n.QuanHuyenID == idquan & n.TenDonVi == tenxaphuong);
            //var link = dataFpt1gatetayninh.DMSOBANNGANHs.SingleOrDefault(n => n.MaSoBanNganh == "PX002");
            return PartialView(link);

            //if (idquan == 19 & tenxaphuong== "Xã Tân Bình")
            //{
            //    var link= dataFptGatemaster.DM_DONVIs.SingleOrDefault(n => n.QuanHuyenID == idquan & n.TenDonVi==tenxaphuong);
            //    //var link = dataFpt1gatetayninh.DMSOBANNGANHs.SingleOrDefault(n => n.MaSoBanNganh == "PX002");
            //    return PartialView(link);
            //}
            //if(idquan == 25 & tenxaphuong == "Xã Tân Bình")
            //{
            //    var link = dataFpt1gatetayninh.DMSOBANNGANHs.SingleOrDefault(n => n.MaSoBanNganh == "PX053");
            //    return PartialView(link);
            //}
            //else
            //{
            //    var link = dataFpt1gatetayninh.DMSOBANNGANHs.SingleOrDefault(n => n.TenSoBanNganh == tenxaphuong);
            //    return PartialView(link);
            //}
        }
        // GET: NopHoSoZaLo/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NopHoSoZaLo/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: NopHoSoZaLo/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: NopHoSoZaLo/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public string send(string MESSAGE, string PHONE)
        {
            //string token = "IIus2HU-VIf89p857RvxAqWgEWWPf3Tw2KOW0oYe52y2M3eOLy4IQ5qtM4i8xqms809DUpBqGJCVFrD-Jlrf1c0UJqCqzMDWEMmhAWIuE61MIIGtK9qfQtvH21vwe3SsIs8dHX_t3t491Jew0SibM38_BZPXhJz9PLmgOcMK0nnoM1LyUjKK4smlF7j9rmuaGZmuUb3nT51aQ6CBJELqML8gFXj_o2zRKZ0LC7t7MKrC86uLOxT2Fc98K65ZfrqmG5STGdwT5W1mR0XeHrHde7aQ7Q1z9G";
            string webAddr = "https://openapi.zalo.me/v2.0/oa/message?access_token=" + tokenZalo;
            //Lấy userid zalo
            string urlgetuser = "https://openapi.zalo.me/v2.0/oa/getprofile?access_token=" + tokenZalo + "&data={%22user_id%22=%22" + PHONE + "%22}";
            var webClient = new System.Net.WebClient();
            var jsoninforamtion = webClient.DownloadString(urlgetuser);
            var messageinfo = JsonConvert.DeserializeObject<Infomation>(jsoninforamtion);

            if (messageinfo.error >= 0)
            {
                var userid = messageinfo.data.user_id;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"recipient\":{\"user_id\":\"" + userid + "\"},\"message\":{\"text\":\"" + MESSAGE + "\"}}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    Console.WriteLine(responseText);
                    var reps = JsonConvert.DeserializeObject<Infomation>(responseText);
                    //luu log
                }
                return "Gửi Msg thành công";
            }
            return messageinfo.message;
        }

        public void sentVNPost(string mahoso,int huyen, string address,string hoten,string didong,string noidung,string ghichu,string noinhan,string diachinhan)
        {
            RestClient client = new RestClient(APILink);
            var request = new RestRequest("/serviceApi/v1/ReceiveOrders?token="+ TokenkeyVNPost, Method.POST);
            var Orders = new List<Order>
                    {
                    new Order ()
                    {
                        PosIdCollect=84,
                        ItemCode="",
                        CustomerCode=CustomerCode,
                        OrderNumber=Guid.NewGuid().ToString().Replace("-",""),
                        CODAmount="",
                        SenderProvince=84,
                        SenderDistrict=huyen,
                        SenderAddress=address,
                        SenderName=hoten,
                        SenderEmail="",
                        SenderTel=didong,
                        SenderDesc=noidung,
                        Description=ghichu,
                        ReceiverName=noinhan,
                        ReceiverAddress=diachinhan,
                        ReceiverTel="",
                        ReceiverProvince=84,
                        ReceiverDistrict="",
                        ReceiverEmail="",
                        ExtendData="",
                        FlagConfig=2
                    }
    };
            request.AddJsonBody(Orders);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(Orders);
            var result = client.Execute(request);
            if (result != null && result.Content != null && result.Content.Length > 0)
            {
            };
        }
        // tạo mẫu KQGQ thủ tục hành chính Sở công thương
        //[HttpGet]
        //public JsonResult TraKetQuaCT(string mabiennhan)
        //{
        //    try
        //    {

        //    var res = dataFpt1gatetayninh.TraKQTT(mabiennhan).FirstOrDefault();
        //    if (res == null)
        //    {
        //        return Json(new { Message = "Không tìm tìm thấy số biên nhận" }, JsonRequestBehavior.AllowGet);
        //    }


        //    string templenamedoc = "_"+mabiennhan+".doc";
        //    string templenamepdf = "_"+mabiennhan+".pdf";
        //    string savePath = Server.MapPath("~/TraCuu/" + templenamedoc);
        //    string templatePath = Server.MapPath("~/TraCuu/MauKQGQ7.docx");
        //    string savePathPdf = Server.MapPath("~/TraCuu/" + templenamepdf);
        //    Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
        //    Application appWord = new Application();

        //    System.IO.FileInfo fileKT = new System.IO.FileInfo(savePathPdf);
        //    if (fileKT.Exists==true)
        //    {
        //        app.Application.Quit();
        //        appWord.Application.Quit();
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileKT.Name + "\"");
        //        Response.AddHeader("Content-Length", fileKT.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(fileKT.FullName);
        //        Response.End();
        //        return Json(new { Message = "" }, JsonRequestBehavior.AllowGet);
        //    }
        //        try
        //    {
        //        Microsoft.Office.Interop.Word.Document doc = new Microsoft.Office.Interop.Word.Document();
        //        doc = app.Documents.Open(templatePath);
        //        doc.Activate();
        //        if (doc.Bookmarks.Exists("LinhVuc"))
        //        {
        //            doc.Bookmarks["LinhVuc"].Range.Text = res.TenLinhVuc;
        //        }
        //        if (doc.Bookmarks.Exists("NgayDaGQ"))
        //        {
        //            doc.Bookmarks["NgayDaGQ"].Range.Text = String.Format("{0:dd/MM/yyyy}", res.NgayDaGiaiQuyet);
        //        }
        //        if (doc.Bookmarks.Exists("NgayHenTra"))
        //        {
        //            doc.Bookmarks["NgayHenTra"].Range.Text = String.Format("{0:dd/MM/yyyy}", res.NgayHenTra);
        //        }
        //        if (doc.Bookmarks.Exists("NgayNhan"))
        //        {
        //            doc.Bookmarks["NgayNhan"].Range.Text = String.Format("{0:dd/MM/yyyy}", res.NgayNhan);
        //        }
        //        if (doc.Bookmarks.Exists("SoBienNhan"))
        //        {
        //            doc.Bookmarks["SoBienNhan"].Range.Text = mabiennhan;
        //        }
        //        if (doc.Bookmarks.Exists("TenDonVi"))
        //        {
        //            doc.Bookmarks["TenDonVi"].Range.Text = res.TenKhachHang;
        //        }
        //        if (doc.Bookmarks.Exists("ThuTuc"))
        //        {
        //            doc.Bookmarks["ThuTuc"].Range.Text = res.TenLoaiHoSo;
        //        }

        //        doc.SaveAs2(savePath);
        //        app.Application.Quit();

        //        //save PDF
        //        Microsoft.Office.Interop.Word.Document wordDocument = new Microsoft.Office.Interop.Word.Document();
        //        wordDocument = appWord.Documents.Open(savePath);
        //        wordDocument.ExportAsFixedFormat(savePathPdf, WdExportFormat.wdExportFormatPDF);
        //        appWord.Application.Quit();

        //        System.IO.FileInfo file = new System.IO.FileInfo(savePathPdf);
        //        Response.Clear();
        //        Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");
        //        Response.AddHeader("Content-Length", file.Length.ToString());
        //        Response.ContentType = "application/octet-stream";
        //        Response.WriteFile(file.FullName);
        //        Response.End();
        //        return Json(new { Message = "" }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        app.Application.Quit();
        //        appWord.Application.Quit();
        //        return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        private void FillBookmarksUsingOpenXml(string sourceDoc, string destDoc, Dictionary<string, string> bookmarkData)
        {
            string wordmlNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
            // Make a copy of the template file.
            System.IO.File.Copy(sourceDoc, destDoc, true);

            //Open the document as an Open XML package and extract the main document part.
            using (WordprocessingDocument wordPackage = WordprocessingDocument.Open(destDoc, true))
            {
                MainDocumentPart part = wordPackage.MainDocumentPart;

                //Setup the namespace manager so you can perform XPath queries 
                //to search for bookmarks in the part.
                NameTable nt = new NameTable();
                XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
                nsManager.AddNamespace("w", wordmlNamespace);

                //Load the part's XML into an XmlDocument instance.
                XmlDocument xmlDoc = new XmlDocument(nt);
                xmlDoc.Load(part.GetStream());

                //Iterate through the bookmarks.
                foreach (KeyValuePair<string, string> bookmarkDataVal in bookmarkData)
                {
                    var bookmarks = from bm in part.Document.Body.Descendants<BookmarkStart>()
                                    select bm;

                    foreach (var bookmark in bookmarks)
                    {
                        if (bookmark.Name == bookmarkDataVal.Key)
                        {
                            Run bookmarkText = bookmark.NextSibling<Run>();
                            if (bookmarkText != null)  // if the bookmark has text replace it
                            {
                                bookmarkText.GetFirstChild<Text>().Text = bookmarkDataVal.Value;
                            }
                            else  // otherwise append new text immediately after it
                            {
                                var parent = bookmark.Parent;   // bookmark's parent element

                                Text text = new Text(bookmarkDataVal.Value);
                                Run run = new Run(new RunProperties());
                                run.Append(text);
                                // insert after bookmark parent
                                parent.Append(run);
                            }

                            //bk.Remove();    // we don't want the bookmark anymore
                        }
                    }
                }

                //Write the changes back to the document part.
                xmlDoc.Save(wordPackage.MainDocumentPart.GetStream(FileMode.Create));
            }
        }
        public JsonResult _TraKetQuaCT(string mabiennhan)
        {
            //declare Url file
            string templenamedoc = "_" + mabiennhan + ".doc";
            string templenamepdf = "_" + mabiennhan + ".pdf";
            string savePath = Server.MapPath("~/TraCuu/WordDocument/" + templenamedoc);
            string templatePath = Server.MapPath("~/TraCuu/MauKQGQ7.docx");
            string savePathPdf = Server.MapPath("~/TraCuu/PDFExport/");
            var res = dataFpt1gatetayninh.TraKQTT(mabiennhan).FirstOrDefault();
            if (res == null)
            {
                return Json(new { Message = "Số biên nhận không đúng" }, JsonRequestBehavior.AllowGet);
            }
            //Kiểm tra tồn tại
            System.IO.FileInfo fileKT = new System.IO.FileInfo(savePathPdf+ templenamepdf);
            if (fileKT.Exists == true)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileKT.Name + "\"");
                Response.AddHeader("Content-Length", fileKT.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(fileKT.FullName);
                Response.End();
                return Json(new { Message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                //add bookmark
                Dictionary<string, string> source = new Dictionary<string, string>();
                source.Add("LinhVuc", res.TenLinhVuc);
                source.Add("NgayDaGQ", String.Format("{0:dd/MM/yyyy}", res.NgayDaGiaiQuyet));
                source.Add("NgayHenTra", String.Format("{0:dd/MM/yyyy}", res.NgayHenTra));
                source.Add("NgayNhan", String.Format("{0:dd/MM/yyyy}", res.NgayNhan));
                source.Add("SoBienNhan", mabiennhan);
                source.Add("TenDonVi", res.TenKhachHang);
                source.Add("ThuTuc", res.TenLoaiHoSo);
                FillBookmarksUsingOpenXml(templatePath, savePath, source);

                string fileName = Path.GetFileName(savePath);
                string fileDir = Path.GetDirectoryName(savePath);


                /**
                 * install LibreOffice
                 * accept quyền thư mục trên máy chủ IIS (everyone,IIS_IUSRS)
                 * C:\Windows\SysWOW64\config\systemprofile\AppData\Roaming\LibreOffice
                 * https://stackoverflow.com/questions/51901973/using-libreofficesoffice-exe-as-process-start-from-code-behind-not-working-o
                 * **/

                var pdfProcess = new Process();
                pdfProcess.StartInfo.Verb = "runas";
                pdfProcess.StartInfo.FileName = @"C:\Program Files (x86)\LibreOffice\program\swriter.exe";
                pdfProcess.StartInfo.Arguments =
                    String.Format("--headless --convert-to pdf:writer_pdf_Export --outdir \"{0}\"  \"{1}\""
                                          , Server.MapPath("~/TraCuu/PDFExport"), savePath);
                //pdfProcess.StartInfo.WorkingDirectory = fileDir;
                pdfProcess.StartInfo.RedirectStandardOutput = true;
                pdfProcess.StartInfo.RedirectStandardError = true;
                pdfProcess.StartInfo.UseShellExecute = false;
                pdfProcess.Start();

                string output = pdfProcess.StandardOutput.ReadToEnd();
                string error = pdfProcess.StandardError.ReadToEnd();

                //Download file
                System.IO.FileInfo file = new System.IO.FileInfo(savePathPdf + templenamepdf);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");
                Response.AddHeader("Content-Length", file.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(file.FullName);
                Response.End();

                //Application appWord = new Application();
                //Microsoft.Office.Interop.Word.Document wordDocument = new Microsoft.Office.Interop.Word.Document();
                //wordDocument = appWord.Documents.Open(savePath);
                //try
                //{
                //    wordDocument = appWord.Documents.Open(savePath);
                //    wordDocument.ExportAsFixedFormat(savePathPdf, WdExportFormat.wdExportFormatPDF);
                //    appWord.Application.Quit();
                //}catch (Exception ex)
                //{
                //    appWord.Application.Quit();
                //    return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
                //}



                //    try
                //{
                //    using (Stream file = System.IO.File.Create(savePathPdf))
                //    {
                //        Converter converter = new Converter("http://converter-eval.plutext.com/v1/00000000-0000-0000-0000-000000000000/convert");
                //        // you should install your own local instance, and point to that
                //        converter.convert(System.IO.File.ReadAllBytes(savePath), Format.DOCX, file, Format.PDF);
                //    }
                //    Console.WriteLine("done!");
                //}
                //catch (ConversionException e)
                //{
                //    Console.WriteLine(e);
                //}

                //Word2Pdf objWorPdf = new Word2Pdf();
                //string FileExtension = Path.GetExtension(templenamedoc);
                ////string ChangeExtension = templenamedoc.Replace(FileExtension, ".pdf");
                //if (FileExtension == ".doc" || FileExtension == ".docx")
                //{
                //    objWorPdf.InputLocation = savePath;
                //    objWorPdf.OutputLocation = savePathPdf;
                //    objWorPdf.Word2PdfCOnversion();
                //    //Download file
                //    System.IO.FileInfo file = new System.IO.FileInfo(savePathPdf);
                //    Response.Clear();
                //    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");
                //    Response.AddHeader("Content-Length", file.Length.ToString());
                //    Response.ContentType = "application/octet-stream";
                //    Response.WriteFile(file.FullName);
                //    Response.End();
                //}
                return Json(new { message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
