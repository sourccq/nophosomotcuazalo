﻿namespace NopHoSoMotCuaZaLo.Models
{
    internal class Order
    {
        public Order()
        {
        }

        public int PosIdCollect { get; set; }
        public string ItemCode { get; set; }
        public string CustomerCode { get; set; }
        public string OrderNumber { get; set; }
        public object CODAmount { get; set; }
        public int SenderProvince { get; set; }
        public int SenderDistrict { get; set; }
        public string SenderAddress { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderTel { get; set; }
        public string SenderDesc { get; set; }
        public string Description { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverTel { get; set; }
        public int ReceiverProvince { get; set; }
        public object ReceiverDistrict { get; set; }
        public string ReceiverEmail { get; set; }
        public string ExtendData { get; set; }
        public int FlagConfig { get; set; }
    }
}