﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NopHoSoMotCuaZaLo.Models
{
    public class ZaloInfomation
    {
        public class Infomation
        {
            public int error { get; set; }
            public string message { get; set; }
            public dataInfo data { get; set; }
        }
        public class dataInfo
        {
            public int user_gender { get; set; }
            public long user_id { get; set; }
            public long user_id_by_app { get; set; }
            public avatars avatars { get; set; }
            public string avatar { get; set; }
            public string display_name { get; set; }
            public int birth_date { get; set; }
            public string shared_info { get; set; }
        }
        public class avatars
        {
            public string avatar120 { get; set; }
            public string avatar240 { get; set; }
        }
    }
}