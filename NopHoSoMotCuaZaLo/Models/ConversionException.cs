﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NopHoSoMotCuaZaLo.Models
{
    public class ConversionException : Exception
    {
        public ConversionException()
        {
        }

        public ConversionException(string message)
            : base(message)
        {
        }

        public ConversionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}